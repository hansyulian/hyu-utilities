describe("Class: Synchronization", function () {
    var HY = require("../../../dist/hy-node");

    var Synchronization = HY.Class.Synchronization;

    it("simple synchronization 1", function () {
        var resultContext = {};
        var expectationContext = {
            pipe1: 1,
            pipe2: 2
        };
        var synchronization = Synchronization(pipe1, pipe2);
        synchronization.done(function (context, next) {
            resultContext = context;
            expect(expectationContext).toEqual(resultContext);
        });

    })


    function pipe1(context, next) {
        setTimeout(function () {
            context.pipe1 = 1;
            next();
        }, 50);
    }

    function pipe2(context, next) {
        context.pipe2 = 2;
        next();
    }
});