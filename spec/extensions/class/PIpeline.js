describe("Class: Pipeline", function () {
    var HY = require("../../../dist/hy-node");
    var Pipeline = HY.Class.Pipeline;

    it("simple pipeline 1", function () {
        var resultContext = {};
        var expectationContext = {
            pipe1: 1,
            pipe2: 2
        };
        var pipeline = Pipeline(pipe1, pipe2, function (context, next) {
            resultContext = context;
            expect(expectationContext).toEqual(resultContext);
        });

    })

    it("simple pipeline 2", function () {
        var resultContext = {};
        var expectationContext = {
            pipe1: 1,
            pipe2: 2
        };
        var pipeline = Pipeline();
        pipeline.then(pipe1, pipe2, function (context, next) {
            resultContext = context;
            expect(expectationContext).toEqual(resultContext);
        });

    })

    it("simple pipeline 3", function () {
        var resultContext = {};
        var expectationContext = {
            pipe1: 1,
            pipe2: 2
        };
        var pipeline = Pipeline();
        pipeline.then(pipe1, pipe2).then(function (context, next) {
            resultContext = context;
            expect(expectationContext).toEqual(resultContext);
        });

    })







    function pipe1(context, next) {
        setTimeout(function () {
            context.pipe1 = 1;
            next();
        }, 50);
    }

    function pipe2(context, next) {
        context.pipe2 = 2;
        next();
    }
});