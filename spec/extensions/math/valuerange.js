describe("Math: Value Range", function () {
	var HY = require("../../../dist/hy-node");
	it("simple value range", function () {
		var groups = [{
				from: 0,
				to: 0.3,
				value: "a"
			},
			{
				from: 0.3,
				to: 0.7,
				value: "b"
			},
			{
				from: 0.7,
				to: 1,
				value: "c"
			},
		]
		expect(HY.Math.valueRange(35, 0, 100, groups)).toEqual("b");
	})
});