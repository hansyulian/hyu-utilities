describe("String: Render", function () {
	var HY = require("../../../dist/hy-node");
	it("simple render", function () {
		var string = "Test {{a}} {{b}} {{c}} {{a}}";
		expect(HY.String.render(string, {
			a: "counter 1",
			b: "counter 2",
			c: "counter 3",
		})).toEqual("Test counter 1 counter 2 counter 3 counter 1");
	})
});