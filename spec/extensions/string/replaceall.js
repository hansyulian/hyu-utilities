describe("String: Replace All", function () {
	var HY = require("../../../dist/hy-node");
	it("simple replace all", function () {
		var string = "Test test Test test";
		expect(HY.String.replaceAll(string, "test", "Yay")).toEqual("Test Yay Test Yay");
	})
});