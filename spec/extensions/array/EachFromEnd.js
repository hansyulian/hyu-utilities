describe("Array: Each From End", function () {
    var HY = require("../../../dist/hy-node");
    it("simple descending", function () {
        var arr1 = [1, 2, 3, 4];
        test = "";
        result = "4321";
        HY.Array.eachFromEnd(arr1, function (element) {
            test += element;
        })
        expect(test).toEqual(result);
    })
});