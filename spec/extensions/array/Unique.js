describe("Array: Unique", function () {
    var HY = require("../../../dist/hy-node");
    it("simple unique", function () {
        var arr1 = [1, 2, 3, 4, 1, 2, 3, 5];
        var arr2 = [1, 2, 3, 4, 5];
        expect(HY.Array.unique(arr1)).toEqual(arr2);
    })
});