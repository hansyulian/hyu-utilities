describe("Array: Reverse", function () {
	var HY = require("../../../dist/hy-node");
	it("simple reverse", function () {
		var arr1 = [1, 2, 3, 4];
		var arr2 = [4, 3, 2, 1];
		expect(HY.Array.reverse(arr1)).toEqual(arr2);
	})
});