describe("Array: Each From Start", function () {
    var HY = require("../../../dist/hy-node");
    it("simple ascending", function () {
        var arr1 = [1, 2, 3, 4];
        test = "";
        result = "1234";
        HY.Array.eachFromStart(arr1, function (element) {
            test += element;
        })
        expect(test).toEqual(result);
    })
});