var gulpWorker = require("gulp-worker");

var scripts = [
    "main.js",
    "core/**/*.js",
    "extensions/**/*.js"
];

var forNode = scripts.concat(["node.js"]);
var forNodeInjected = forNode.concat(["inject.js"]);
var forBrowserInjected = scripts.concat(["browser.js", "inject.js"]);

gulpWorker.js(scripts, {
    destination: "dist",
    baseFolder: "src",
    versionOnFile: false,
    name: "hy",
    generateSourceMaps: false,
});
gulpWorker.js(forNode, {
    destination: "dist",
    baseFolder: "src",
    versionOnFile: false,
    name: "hy-node",
    generateSourceMaps: false,
});
gulpWorker.js(forBrowserInjected, {
    destination: "dist",
    baseFolder: "src",
    versionOnFile: false,
    name: "hy-inject",
    generateSourceMaps: false,
});
gulpWorker.js(forNodeInjected, {
    destination: "dist",
    baseFolder: "src",
    versionOnFile: false,
    name: "hy-node-inject",
    generateSourceMaps: false,
});