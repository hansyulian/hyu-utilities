var HY = (function (HY) {

	function Synchronization() {
		var args = arguments;
		return (function (args) {

			var syncronization = {
				processes: [],
				onDone: [],
				context: {},
				remainingProcess: 0,

				addProcess: addProcess,
				restart: restart,
				done: done,
			}
			var processes = syncronization.processes;
			var onDone = syncronization.onDone;
			var context = syncronization.context;

			for (var i in args)
				addProcess(args[i]);
			restart();

			function addProcess(process) {
				if (typeof (process) == "function") {
					processes.push(process);

				}
			}


			function executeAll() {
				for (var i in processes) {
					execute(processes[i]);
				}
			}

			function execute(process) {
				process(context, finished)
			}

			function finished() {
				syncronization.remainingProcess--;
				checkStates();
			}

			function checkStates() {
				if (syncronization.remainingProcess == 0)
					allProcessFinished();
			}

			function allProcessFinished() {
				for (var i in onDone) {
					onDone[i](context);
				}
			}

			function restart() {
				syncronization.remainingProcess = processes.length;
				executeAll();
			}

			function done() {
				var callbacks = arguments;
				for (var i in callbacks) {
					if (typeof (callbacks[i]) == "function") {
						if (syncronization.remainingProcess == 0)
							callbacks[i](context);
						onDone.push(callbacks[i]);
					}
				}
				return syncronization;
			}

			return syncronization;
		})(args);

	};
	HY.Class.Synchronization = Synchronization;
	return HY;

	/*
	var synchronization = Synchronization(s1,s2,s3,s4);
	synchronization.done(done1,done2,done3);

	var s1 = s2 = s3 = s4 = function(context,next){
		context.value = context.value + 1 || 0;
		next();
	}

	var done1 = done2 = done3 = function(context){
		console.log(context.value);
	}
	*/
})(HY);