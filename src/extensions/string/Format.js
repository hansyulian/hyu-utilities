var HY = (function (HY) {

	HY.String.format = function () {
		var result = arguments[0];
		for (var i = 1; i < arguments.length; i++) {
			var reg = new RegExp("\\{" + (i - 1) + "\\}", "g");
			result = result.replace(reg, arguments[i]);
		};
		return result;
	};
	return HY;
})(HY);