var HY = (function (HY) {

	HY.String.replaceAll = function (string, find, replace) {
		var reg = new RegExp(find, "g");
		return string.replace(reg, replace);
	};
	return HY;
})(HY);