(function (HY) {

    /*compare multiple objects 
     */
    HY.Utility.queryStrings = function (query) {
        if (query)
            return createQueryString(query);
        else
            return getQueryString();
    };

    function createQueryStrings(query) {
        var result = "";
        var first = true;
        for (var name in query) {
            if (first)
                first = !first;
            else
                result += "&";
            result += name + "=" + query[name];
        }
        return result;
    }

    function getQueryStrings() {

        var url = document.location;
        var queryString = url.search.substring(1);
        if (queryString == "")
            return {};
        var queries = queryString.split("&");
        var result = {};
        for (var i = 0; i < queries.length; i++) {
            var splitting = queries[i].split("=");
            var name = splitting[0];
            var value = splitting[1];
            result[name] = value;
        }
        return result;
    }
    return HY;
})(HY);