(function (HY) {

    HY.Utility.LoadJS = function (url, callback) {
        callback = callback || function () {};
        var fileref = document.createElement('script')
        fileref.setAttribute("type", "text/javascript")
        fileref.setAttribute("src", url);
        fileref.onload = callback;
        document.head.appendChild(fileref);
    };;
    return HY;
})(HY);