(function (HY) {

	HY.Utility.namespace = function (name) {
		var segments = name.split(".");
		var result = window[segments[0]] = window[segments[0]] || {};
		for (var i = 1; i < segments.length; i++) {
			if (!(segments[i] in result))
				result[segments[i]] = {};
			result = result[segments[i]];
		}
		return result;
	};
	return HY;
})(HY);