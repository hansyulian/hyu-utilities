(function (HY) {

	HY.Utility.isSame = function (left, right) {
		function reccursiveSame(left, right) {
			leftType = typeof (left) == "object";
			rightType = typeof (right) == "object";
			if (leftType && rightType) {
				var result = true;
				if (Object.keys(left).length != Object.keys(right).length)
					return false;
				for (var i in left) {
					result = result && reccursiveSame(left[i], right[i]);
				}
				return result;
			} else
				return left == right
		}
		return reccursiveSame(left, right);
	};
	return HY;
})(HY);