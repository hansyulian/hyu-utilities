(function (HY) {

    /*compare multiple objects 
     */
    HY.Utility.compare = function (objects) {
        var result = {};
        var length = objects.length;

        //prepare to init with undefined values
        //"set the value for column $name row $key with value of $value, if not $key not exists, then make new one with default of each name as undefined"
        var setDefaultIfNotExists = function (parameter) {
            if (parameter in result)
                return;
            var value = [];
            for (var i = 0; i < length; i++)
                value[i] = undefined;
            result[parameter] = value;
        }

        for (var i = 0; i < length; i++) {
            values = objects[i];
            for (var key in values) {
                setDefaultIfNotExists(key);
                result[key][i] = values[key]
            }
        }

        result._difference = {};
        for (var i in result) {
            var same = true;
            for (var j = 0; j < result[i].length - 1; j++) {
                same = same && result[i][j] == result[i][j + 1]
            }
            if (!same)
                result._difference[i] = result[i];
        }

        return result;
    };
    return HY;
})(HY);