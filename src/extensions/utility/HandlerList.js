(function (HY) {
    //loop trough the handler list;
    HY.Utility.handle = function (value, handlers, options) {
        var options = options || {};

        var ascending = options.ascending;

        //defautl is descending
        var increment = 1;
        start = handlers.length - 1;
        end = 0;
        //swap if ascending
        if (ascending) {
            increment = -1;
            start = [end, end = start][0];
        }


        var result = null;
        for (var i = start; start != end && !result; i += increment)
            result = handlers[i](value);
        return result;
    };
    return HY;
})(HY);