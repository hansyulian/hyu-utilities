var HY = (function(HY) {

    HY.Object.get = function(object, stringQuery) {
        var segments = stringQuery.split(".");
        var result = object;
        for (var i = 0; i < segments.length && result; i++) {
            result = result[segments[i]];
        }
        return result;
    };
    return HY;
})(HY);