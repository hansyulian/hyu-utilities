var HY = (function (HY) {

	HY.Math.valueRangeScale = function (value, ranges) {
		for (var i = 0; i < ranges.length; i++)
			if (ranges[i].from <= value && ranges[i].to >= value)
				return ranges[i].value;
		return null;
	};
	return HY;
})(HY);