var HY = (function(HY) {

    HY.Math.scale = function(value, min, max) {
        return (value + min) / (min + max);
    };
    return HY;
})(HY);