var HY = (function(HY) {
    //reverse array content
    HY.Array.reverse = function(arr) {
        var result = [];
        for (var i = arr.length - 1; i >= 0; i--) {
            result.push(arr[i]);
        }
        return result;
    };
    return HY;
})(HY);