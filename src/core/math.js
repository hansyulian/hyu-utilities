(function (HY) {

	var HYM = HY.Math = HY.Math || {};

	HYM.__injector__ = function () {
		HY.__injector__(Math, HYM);
	}
	return HY;
})(HY);