(function (HY) {

	var HYC = HY.Class = HY.Class || {};


	HYC.__injector__ = function () {
		HY.__object_injector__(HY.globalScope, HYC);
	}
	return HY;
})(HY);