(function (HY) {

	var HYA = HY.Array = HY.Array || {};

	HYA.__injector__ = function () {
		HY.__object_injector__(Array.prototype, HYA);
	}
	return HY;
})(HY);