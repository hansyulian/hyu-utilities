var HY = (function () {
	var HY = {};
	HY.version = "0.2.0";

	HY.settings = {

	};

	HY.__object_injector__ = function (target, namespace) {
		for (var i in namespace) {
			if (typeof (namespace[i]) == "function" && !i.startsWith("_"))
				target[i] = (function (scopeFunction) {
					return function () {
						var parameters = [this];
						for (var i = 0; i < arguments.length; i++) {
							parameters.push(arguments[i]);
						}
						return scopeFunction.apply(this, parameters);
					}
				})(namespace[i])
		}
	};
	HY.__injector__ = function (target, namespace) {
		for (var i in namespace) {
			if (typeof (namespace[i]) == "function" && !i.startsWith("_"))
				target[i] = (function (scopeFunction) {
					return scopeFunction;
				})(namespace[i]);
		}
	}

	HY.inject = function () {
		for (var i in HY) {
			if (typeof (HY[i]) == "object" && "__injector__" in HY[i])
				HY[i].__injector__();
		}
	}
	return HY;
})();